import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from tensorflow import keras

if __name__ == '__main__':
    # DATA
    data_df = pd.read_csv("./data.csv")

    inputs = []
    labels = []

    for i, row in data_df.iterrows():
        inputs.append([row["v1"], row["v2"]])
        labels.append([row["sum"]])

    inputs = np.array(inputs)
    labels = np.array(labels)

    # Split dataset to training and validation 50% each
    split = int(len(inputs) / 2)

    training_inputs = inputs[:split]
    training_labels = labels[:split]

    val_inputs = inputs[split:]
    val_labels = labels[split:]

    # TRAINING

    # Model contains layers
    # Dense layer, is composed of neurons

    model = keras.models.Sequential([
        keras.layers.InputLayer(input_shape=(2,)),
        keras.layers.Dense(2, activation=keras.activations.linear),
        keras.layers.Dense(1, activation=keras.activations.linear),
    ])

    # Compile the model
    model.compile(
        optimizer=keras.optimizers.Adam(),
        loss=keras.losses.mean_squared_error,
        metrics=[keras.metrics.mean_squared_error],
    )

    # Now the model is ready to be trained
    # we use model.fit for that purpose

    history = model.fit(x=inputs, y=labels, epochs=15)
    # Think of epochs like a race with a circular track
    # Each epoch, the cars have to run on the same track
    # Tensorflow will run over the same dataset multiple epochs,
    # each time it modifies the weights of the layers to try and have lower loss

    # Model is trained, now let's validate

    err = history.history["mean_squared_error"]

    epochs = range(1, len(err) + 1)

    plt.clf()  # clear figure

    plt.plot(epochs, err, 'b', label='Training err')
    plt.title('Training Error')
    plt.xlabel('Epochs')
    plt.ylabel('Accuracy')
    plt.legend()
    axes = plt.gca()

    plt.show()

    evaluation = model.evaluate(x=val_inputs, y=val_labels)
    validation_loss = evaluation[0]
    validation_err = evaluation[1]
    print("Validation loss = {}, Validation error = {}".format(validation_loss, validation_err))

    print("let's try some numbers !")

    prediction_inputs = np.array([
        [1, 2],  # should be equal to 3
        [1000, 2000],  # should be equal to 3,000
        [5000, 5000],  # should be equal to 10,000
        [9000, 9000],  # This is beyond the dataset ! let's see the prediction
        [-1000, -9000],  # Let's try something negative too, will the prediction hit or miss ?
    ])
    prediction_labels = model.predict(x=prediction_inputs).round(decimals=1)

    for i in range(len(prediction_inputs)):
        print("prediction of {} + {} = {}".format(
            prediction_inputs[i][0], prediction_inputs[i][1], prediction_labels[i][0],
        ))

    # THAT's IT !
    # Now try modifying the model, try adding more layers,
    # try playing with the parameters of Dense layer to achieve better results
