import random

import pandas as pd

min_val, max_val = 0, 5000

if __name__ == '__main__':
    data = []
    for i in range(20000):
        v1 = random.randint(min_val, max_val)
        v2 = random.randint(min_val, max_val)
        data.append([v1, v2, v1 + v2])

    df = pd.DataFrame.from_records(data, columns=["v1", "v2", "sum"])
    df.to_csv("./data.csv", index=False)
